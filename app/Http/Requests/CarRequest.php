<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CarRequest extends FormRequest
{
   
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->route()->getActionMethod()) {
            case 'findCar':
                return $this->findCarRules();
            case 'book':
                return $this->bookRules();
            default:
                return [];
        }

        return [];
    }

    /**
     * Get the validation rules that apply to storing a new model resource.
     *
     * @return array
     */
    public function findCarRules() : array
    {
        return [
            'lat' => 'required|',
            'long' => 'required',
            'distance' => 'required',
        ];
    }

    /**
     * Get the validation rules that apply to storing a new model resource.
     *
     * @return array
     */
    public function bookRules() : array
    {
        return [
            'lat' => 'required|',
            'long' => 'required',
            'distance' => 'required',
        ];
    }
}
